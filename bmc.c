#include "bmc.h"

#include <assert.h>
#include <windows.h>

// ----------------------------------------------------------------------------
// 
// C common
// 
// ----------------------------------------------------------------------------

#define BMC_STRLEN(s) strlen((s))
#define BMC_WCSLEN(s) wcslen((s))
#define BMC_MALLOC(s) malloc((s))
#define BMC_FREE(s)   free((s))

#define BMC_CAT(A, B) BMC_CAT_(A, B)
#define BMC_CAT_(A, B) A ## B

#define BMC_STATIC_ASSERT(e) \
	enum { BMC_CAT(static_assert_, __LINE__) = 1/(!!(e)) }

#define BMC_ARRAY_COUNTOF(arr)\
	(sizeof(arr) / sizeof(arr[0]))

// ----------------------------------------------------------------------------
// 
// Static common
// 
// ----------------------------------------------------------------------------

static short s_int_to_short(int v)
{
	assert(v >= SHRT_MIN && v <= SHRT_MAX);
	return (short)v;
}

static WORD s_convert_fg_color(BMC_COLOR color)
{
	WORD w_colors[] = {
		0,
		FOREGROUND_RED  ,
		FOREGROUND_GREEN,
		FOREGROUND_BLUE ,
		FOREGROUND_RED  | FOREGROUND_GREEN ,
		FOREGROUND_RED  | FOREGROUND_BLUE  ,
		FOREGROUND_BLUE | FOREGROUND_GREEN ,
		FOREGROUND_RED  | FOREGROUND_GREEN | FOREGROUND_BLUE,
		0,
		FOREGROUND_INTENSITY | FOREGROUND_RED  ,
		FOREGROUND_INTENSITY | FOREGROUND_GREEN,
		FOREGROUND_INTENSITY | FOREGROUND_BLUE ,
		FOREGROUND_INTENSITY | FOREGROUND_RED  | FOREGROUND_GREEN ,
		FOREGROUND_INTENSITY | FOREGROUND_RED  | FOREGROUND_BLUE  ,
		FOREGROUND_INTENSITY | FOREGROUND_BLUE | FOREGROUND_GREEN ,
		FOREGROUND_INTENSITY | FOREGROUND_RED  | FOREGROUND_GREEN | FOREGROUND_BLUE,
		FOREGROUND_INTENSITY
	};
	BMC_STATIC_ASSERT(BMC_COLOR__COUNT == BMC_ARRAY_COUNTOF(w_colors));
	assert(color >= 0 && color < BMC_ARRAY_COUNTOF(w_colors));
	return w_colors[color];
}

static WORD s_convert_bg_color(BMC_COLOR color)
{
	WORD w_colors[] = {
		0,
		BACKGROUND_RED  ,
		BACKGROUND_GREEN,
		BACKGROUND_BLUE ,
		BACKGROUND_RED  | BACKGROUND_GREEN ,
		BACKGROUND_RED  | BACKGROUND_BLUE  ,
		BACKGROUND_BLUE | BACKGROUND_GREEN ,
		BACKGROUND_RED  | BACKGROUND_GREEN | BACKGROUND_BLUE,
		0,
		BACKGROUND_INTENSITY | BACKGROUND_RED  ,
		BACKGROUND_INTENSITY | BACKGROUND_GREEN,
		BACKGROUND_INTENSITY | BACKGROUND_BLUE ,
		BACKGROUND_INTENSITY | BACKGROUND_RED  | BACKGROUND_GREEN ,
		BACKGROUND_INTENSITY | BACKGROUND_RED  | BACKGROUND_BLUE  ,
		BACKGROUND_INTENSITY | BACKGROUND_BLUE | BACKGROUND_GREEN ,
		BACKGROUND_INTENSITY | BACKGROUND_RED  | BACKGROUND_GREEN | BACKGROUND_BLUE,
		BACKGROUND_INTENSITY
	};
	BMC_STATIC_ASSERT(BMC_COLOR__COUNT == BMC_ARRAY_COUNTOF(w_colors));
	assert(color >= 0 && color < BMC_ARRAY_COUNTOF(w_colors));
	return w_colors[color];
}

// ----------------------------------------------------------------------------
// 
// Static core data
// 
// ----------------------------------------------------------------------------

typedef struct {
	BMC_BOOL is_multicore;
	BMC_BOOL high_perf_support;
	LARGE_INTEGER high_perf_freq;
	unsigned seed;
} CoreData;
static CoreData s_core;

static BMC_BOOL s_init_core()
{
	SYSTEM_INFO sys_info;
	GetSystemInfo(&sys_info);
	s_core.is_multicore = (sys_info.dwNumberOfProcessors > 1);
	s_core.high_perf_support = (TRUE == QueryPerformanceFrequency(&s_core.high_perf_freq));
	s_core.seed = 0;
	return BMC_TRUE;
}

static void s_deinit_core()
{
}

// ----------------------------------------------------------------------------
// 
// Static console data
// 
// ----------------------------------------------------------------------------

typedef struct {
	HANDLE handle;
	CHAR_INFO* buffer;
	short cursor_pos;
	BMC_COLOR fg_color;
	BMC_COLOR bg_color;
	COORD buffer_size;
	short buffer_area;
	SMALL_RECT console_rect;
} ConsoleData;
static ConsoleData s_console;

static BMC_BOOL s_init_console(short sw, short sh, unsigned cp)
{
	assert(sw > 0 && sh > 0);
	s_console.handle = 0;
	s_console.buffer = NULL;
	s_console.cursor_pos = 0;
	s_console.fg_color = BMC_COLOR_WHITE;
	s_console.bg_color = BMC_COLOR_BLACK;
	s_console.buffer_size.X = sw;
	s_console.buffer_size.Y = sh;
	s_console.buffer_area = sw * sh;
	s_console.console_rect.Left = 0;
	s_console.console_rect.Top = 0;
	s_console.console_rect.Right = sw - 1;
	s_console.console_rect.Bottom = sh - 1;
	{
		s_console.handle = GetStdHandle(STD_OUTPUT_HANDLE);
		if ( INVALID_HANDLE_VALUE == s_console.handle ) {
			return BMC_FALSE;
		}
	}
	{
		CONSOLE_CURSOR_INFO cursor_info;
		if ( FALSE == GetConsoleCursorInfo(s_console.handle, &cursor_info) ) {
			return BMC_FALSE;
		}
		cursor_info.bVisible = FALSE;
		if ( FALSE == SetConsoleCursorInfo(s_console.handle, &cursor_info) ) {
			return BMC_FALSE;
		}
	}
	{
		s_console.buffer = BMC_MALLOC(sizeof(CHAR_INFO) * sw * sh);
		if ( NULL == s_console.buffer ) {
			return BMC_FALSE;
		}
	}
	{
		CONSOLE_SCREEN_BUFFER_INFO console_info;
		if ( FALSE == GetConsoleScreenBufferInfo(s_console.handle, &console_info) ) {
			return BMC_FALSE;
		}
		if ( console_info.dwSize.X < sw || console_info.dwSize.Y < sh ) {
			if ( FALSE == SetConsoleScreenBufferSize(s_console.handle, s_console.buffer_size) ) {
				return BMC_FALSE;
			}
		}
		if ( FALSE == SetConsoleWindowInfo(s_console.handle, TRUE, &s_console.console_rect) ) {
			return BMC_FALSE;
		}
		if ( FALSE == SetConsoleScreenBufferSize(s_console.handle, s_console.buffer_size) ) {
			return BMC_FALSE;
		}
	}
	if ( 0 != cp ) {
		if ( FALSE == SetConsoleCP(cp) ) {
			return BMC_FALSE;
		}
		if ( FALSE == SetConsoleOutputCP(cp) ) {
			return BMC_FALSE;
		}
	}
	return BMC_TRUE;
}

static void s_deinit_console()
{
	if ( s_console.buffer ) {
		BMC_FREE(s_console.buffer);
		s_console.buffer = NULL;
	}
}

static void s_change_buffer_chr(short index, char chr)
{
	assert(index >= 0 && index < s_console.buffer_area);
	s_console.buffer[index].Attributes =
		s_convert_fg_color(s_console.fg_color) |
		s_convert_bg_color(s_console.bg_color);
	s_console.buffer[index].Char.AsciiChar = chr;
}

static void s_change_buffer_wchr(short index, wchar_t chr)
{
	assert(index >= 0 && index < s_console.buffer_area);
	s_console.buffer[index].Attributes =
		s_convert_fg_color(s_console.fg_color) |
		s_convert_bg_color(s_console.bg_color);
	s_console.buffer[index].Char.UnicodeChar = chr;
}

static BMC_BOOL s_check_key(char* chr, int* vk)
{
	HANDLE h = GetStdHandle(STD_INPUT_HANDLE);
	if ( INVALID_HANDLE_VALUE != h ) {
		DWORD event_count = 0;
		if ( TRUE == GetNumberOfConsoleInputEvents(h, &event_count) ) {
			while ( event_count > 0 ) {
				DWORD numread;
				INPUT_RECORD record;
				if ( TRUE == ReadConsoleInput(h, &record, 1, &numread) ) {
					if ( record.EventType == KEY_EVENT && record.Event.KeyEvent.bKeyDown ) {
						if ( chr ) {
							*chr = record.Event.KeyEvent.uChar.AsciiChar;
						}
						if ( vk ) {
							*vk  = record.Event.KeyEvent.wVirtualKeyCode;
						}
						return BMC_TRUE;
					}
				}
				if ( FALSE == GetNumberOfConsoleInputEvents(h, &event_count) ) {
					break;
				}
			}
		}
	}
	return BMC_FALSE;
}

// ----------------------------------------------------------------------------
// 
// Public
// 
// ----------------------------------------------------------------------------

BMC_BOOL bmc_initialize(int sw, int sh, unsigned cp)
{
	if ( !s_init_core() ) {
		return BMC_FALSE;
	}
	if ( !s_init_console(s_int_to_short(sw), s_int_to_short(sh), cp) ) {
		return BMC_FALSE;
	}
	bmc_clear();
	bmc_blit();
	return BMC_TRUE;
}

void bmc_shutdown()
{
	s_deinit_console();
	s_deinit_core();
}

void bmc_blit()
{
	COORD coord = {0, 0};
	WriteConsoleOutputW(s_console.handle,
		s_console.buffer, s_console.buffer_size,
		coord, &s_console.console_rect);
	bmc_move(0, 0);
}

void bmc_clear()
{
	short i = 0;
	for (; i < s_console.buffer_area; ++i ) {
		s_change_buffer_wchr(i, L' ');
	}
	bmc_move(0,0);
}

void bmc_move(int x, int y)
{
	short sx = s_int_to_short(x);
	short sy = s_int_to_short(y);
	assert(sx >= 0 && sy >= 0);
	assert(sx < s_console.buffer_size.X && sy < s_console.buffer_size.Y);
	s_console.cursor_pos = sy * s_console.buffer_size.X + sx;
}

void bmc_print_chr(char chr)
{
	s_change_buffer_chr(s_console.cursor_pos, chr);
	if ( s_console.cursor_pos < s_console.buffer_area - 1 ) {
		++s_console.cursor_pos;
	}
}

void bmc_print_wchr(wchar_t chr)
{
	s_change_buffer_wchr(s_console.cursor_pos, chr);
	if ( s_console.cursor_pos < s_console.buffer_area - 1 ) {
		++s_console.cursor_pos;
	}
}

void bmc_print_str(const char* str)
{
	size_t i = 0;
	size_t len = str ? BMC_STRLEN(str) : 0;
	for (; i < len; ++i )
		bmc_print_chr(str[i]);
}

void bmc_print_wstr(const wchar_t* str)
{
	size_t i = 0;
	size_t len = str ? BMC_WCSLEN(str) : 0;
	for (; i < len; ++i )
		bmc_print_wchr(str[i]);
}

void bmc_color_fg(BMC_COLOR color)
{
	s_console.fg_color = color;
}

void bmc_color_bg(BMC_COLOR color)
{
	s_console.bg_color = color;
}

void bmc_sleep(unsigned ms)
{
	Sleep(ms);
}

void bmc_wait_key(char* chr, int* vk)
{
	while ( !s_check_key(chr, vk) ) {
		Sleep(0);
		continue;
	}
}

unsigned bmc_get_time()
{
	if ( s_core.high_perf_support ) {
		LARGE_INTEGER perf_counter;
		DWORD_PTR affinity_mask = 0;
		BMC_BOOL success = BMC_FALSE;
		if ( s_core.is_multicore ) {
			affinity_mask = SetThreadAffinityMask(GetCurrentThread(), 1);
		}
		success = (TRUE == QueryPerformanceCounter(&perf_counter));
		if ( s_core.is_multicore ) {
			SetThreadAffinityMask(GetCurrentThread(), affinity_mask);
		}
		if ( success ) {
			return (unsigned)((perf_counter.QuadPart) * 1000 / s_core.high_perf_freq.QuadPart);
		}
	}
	return (unsigned)GetTickCount();
}

void bmc_rnd_seed(unsigned seed)
{
	s_core.seed = seed;
}

int bmc_rnd_int(int min, int max)
{
	s_core.seed = 214013 * s_core.seed + 2531011;
	return min + (s_core.seed ^ s_core.seed >> 15) % (max - min + 1);
}

float bmc_rnd_float(float min, float max)
{
	s_core.seed = 214013 * s_core.seed + 2531011;
	return min + (s_core.seed >> 16) * (1.0f / 65535.0f) * (max - min);
}
