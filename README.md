# Examples #

## Simple blit ##

~~~~
if ( bmc_initialize(80, 25, 0) ) {
  bmc_move(5, 5);
  bmc_print_str("Hello");
  bmc_blit();
  bmc_shutdown();
}
~~~~

## Advanced blit ##

~~~~
if ( bmc_initialize(80, 25, 0) ) {
  bmc_move(5, 5);
  bmc_color_fg(BMC_COLOR_GREEN);
  bmc_color_bg(BMC_COLOR_YELLOW);
  bmc_print_str("Hello");
  bmc_blit();
  bmc_shutdown();
}
~~~~

## Wait user input ##

~~~~
if ( bmc_initialize(80, 25, 0) ) {
  while ( 1 ) {
    int vk = BMC_VK_ESCAPE;
    bmc_move(bmc_rnd_int(0, 79), bmc_rnd_int(0, 24));
    bmc_print_str("Press ESC for exit");
    bmc_blit();
    bmc_wait_key(NULL, &vk);
    if ( BMC_VK_ESCAPE == vk ) {
      break;
    }
  }
  bmc_shutdown();
}
~~~~