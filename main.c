#include "bmc.h"

#define SCREEN_W 80
#define SCREEN_H 25

int main() {
	int vk = BMC_VK_ESCAPE;
	if ( bmc_initialize(SCREEN_W, SCREEN_H, 0) ) {
		while ( 1 ) {
			bmc_move(
				bmc_rnd_int(0, SCREEN_W - 1),
				bmc_rnd_int(0, SCREEN_H - 1));
			bmc_color_fg(
				bmc_rnd_int(BMC_COLOR_NONE + 1, BMC_COLOR__COUNT - 1));
			bmc_color_bg(
				bmc_rnd_int(BMC_COLOR_NONE + 1, BMC_COLOR__COUNT - 1));
			bmc_print_str(
				"Press ESC for exit or other key for continue");
			bmc_blit();
			bmc_wait_key(NULL, &vk);
			if ( BMC_VK_ESCAPE == vk ) {
				break;
			}
		}
		bmc_shutdown();
	}
	return 0;
}
