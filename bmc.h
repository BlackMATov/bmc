#ifndef _BMC_h_
#define _BMC_h_

#include <wchar.h>

#ifdef  __cplusplus
extern "C" {
#endif

typedef enum {
	BMC_FALSE = 0,
	BMC_TRUE  = 1
} BMC_BOOL;

typedef enum {
	BMC_VK_BACKSPACE = 0x08,
	BMC_VK_TAB       = 0x09,
	BMC_VK_RETURN    = 0x0D,
	BMC_VK_ESCAPE    = 0x1B,
	
	BMC_VK_SPACE     = 0x20,
	BMC_VK_PAGE_UP   = 0x21,
	BMC_VK_PAGE_DOWN = 0x22,
	BMC_VK_END       = 0x23,
	BMC_VK_HOME      = 0x24,
	
	BMC_VK_LEFT      = 0x25,
	BMC_VK_UP        = 0x26,
	BMC_VK_RIGHT     = 0x27,
	BMC_VK_DOWN      = 0x28,
	
	BMC_VK_INSERT    = 0x2D,
	BMC_VK_DELETE    = 0x2E,
	
	BMC_VK_0         = 0x30,
	BMC_VK_1         = 0x31,
	BMC_VK_2         = 0x32,
	BMC_VK_3         = 0x33,
	BMC_VK_4         = 0x34,
	BMC_VK_5         = 0x35,
	BMC_VK_6         = 0x36,
	BMC_VK_7         = 0x37,
	BMC_VK_8         = 0x38,
	BMC_VK_9         = 0x39,
	
	BMC_VK_A         = 0x41,
	BMC_VK_B         = 0x42,
	BMC_VK_C         = 0x43,
	BMC_VK_D         = 0x44,
	BMC_VK_E         = 0x45,
	BMC_VK_F         = 0x46,
	BMC_VK_G         = 0x47,
	BMC_VK_H         = 0x48,
	BMC_VK_I         = 0x49,
	BMC_VK_J         = 0x4A,
	BMC_VK_K         = 0x4B,
	BMC_VK_L         = 0x4C,
	BMC_VK_M         = 0x4D,
	BMC_VK_N         = 0x4E,
	BMC_VK_O         = 0x4F,
	BMC_VK_P         = 0x50,
	BMC_VK_Q         = 0x51,
	BMC_VK_R         = 0x52,
	BMC_VK_S         = 0x53,
	BMC_VK_T         = 0x54,
	BMC_VK_U         = 0x55,
	BMC_VK_V         = 0x56,
	BMC_VK_W         = 0x57,
	BMC_VK_X         = 0x58,
	BMC_VK_Y         = 0x59,
	BMC_VK_Z         = 0x5A,
	
	BMC_VK_N0        = 0x60,
	BMC_VK_N1        = 0x61,
	BMC_VK_N2        = 0x62,
	BMC_VK_N3        = 0x63,
	BMC_VK_N4        = 0x64,
	BMC_VK_N5        = 0x65,
	BMC_VK_N6        = 0x66,
	BMC_VK_N7        = 0x67,
	BMC_VK_N8        = 0x68,
	BMC_VK_N9        = 0x69,
	
	BMC_VK_MULTIPLY  = 0x6A,
	BMC_VK_ADD       = 0x6B,
	BMC_VK_SEPARATOR = 0x6C,
	BMC_VK_SUBTRACT  = 0x6D,
	BMC_VK_DECIMAL   = 0x6E,
	BMC_VK_DIVIDE    = 0x6F,
	
	BMC_VK_F1        = 0x70,
	BMC_VK_F2        = 0x71,
	BMC_VK_F3        = 0x72,
	BMC_VK_F4        = 0x73,
	BMC_VK_F5        = 0x74,
	BMC_VK_F6        = 0x75,
	BMC_VK_F7        = 0x76,
	BMC_VK_F8        = 0x77,
	BMC_VK_F9        = 0x78,
	BMC_VK_F10       = 0x79,
	BMC_VK_F11       = 0x7A,
	BMC_VK_F12       = 0x7B
} BMC_VK;

typedef enum {
	BMC_COLOR_NONE,
	
	BMC_COLOR_RED,
	BMC_COLOR_GREEN,
	BMC_COLOR_BLUE,
	BMC_COLOR_YELLOW,
	BMC_COLOR_MAGENTA,
	BMC_COLOR_CYAN,
	BMC_COLOR_WHITE,
	BMC_COLOR_BLACK,
	
	BMC_COLOR_INTENSIVE_RED,
	BMC_COLOR_INTENSIVE_GREEN,
	BMC_COLOR_INTENSIVE_BLUE,
	BMC_COLOR_INTENSIVE_YELLOW,
	BMC_COLOR_INTENSIVE_MAGENTA,
	BMC_COLOR_INTENSIVE_CYAN,
	BMC_COLOR_INTENSIVE_WHITE,
	BMC_COLOR_INTENSIVE_BLACK,
	
	BMC_COLOR__COUNT
} BMC_COLOR;

BMC_BOOL bmc_initialize ( int sw, int sh, unsigned cp );
void     bmc_shutdown   ();

void     bmc_blit       ();
void     bmc_clear      ();
void     bmc_move       ( int x, int y );

void     bmc_print_chr  ( char chr );
void     bmc_print_wchr ( wchar_t chr );
void     bmc_print_str  ( const char* str );
void     bmc_print_wstr ( const wchar_t* str );

void     bmc_color_fg   ( BMC_COLOR color );
void     bmc_color_bg   ( BMC_COLOR color );

void     bmc_sleep      ( unsigned ms );
void     bmc_wait_key   ( char* chr, int* vk );
unsigned bmc_get_time   ();

void     bmc_rnd_seed   ( unsigned seed );
int      bmc_rnd_int    ( int min, int max );
float    bmc_rnd_float  ( float min, float max );

#ifdef  __cplusplus
} // extern "C"
#endif

#endif // _BMC_h_
